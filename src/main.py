from stemmer import *
import re


ENCODING = 'UTF8'


def split_text_into_words(text: str):
    return [word for word in re.split(r'[\s,.!?\n\r()\[\]-]+', text)
            if word is not None and len(word) != 0]


if __name__ == '__main__':
    with open(input('Введите путь к файлу с текстом: '), 'r', encoding=ENCODING) as file:
        text_words = split_text_into_words(file.read())

    stemmed_words = [stem(text_word) for text_word in text_words]
    stemmed_words.sort()

    with (open(input('Введите путь для сохранения файла: '), 'w', encoding=ENCODING)) as file:
        file.write('\n'.join(stemmed_words))
